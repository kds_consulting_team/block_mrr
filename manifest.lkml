constant: connection {
  value: "keboola_block_mrr"
}

# url of your Salesforce domain for object links
constant: domain {
  value: "keboola.lightning.force.com"
}
